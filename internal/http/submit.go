package http

import (
	"../../api"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"github.com/google/certificate-transparency-go/tls"
	"github.com/google/trillian"
	tcrypto "github.com/google/trillian/crypto"
	"io"
	"io/ioutil"
	"net"
	"os"
	"path"
	"strconv"
	"time"
)

// Trillian LogLeaf.ExtraData field
type extraData struct {
	TimestampNanos uint64 `json:"timestamp_nanos"`
}

func now() ([]byte, error) {
	extraData := extraData{
		TimestampNanos: uint64(time.Now().UnixNano()),
	}

	jsonExtraData, err := json.Marshal(extraData)
	if err != nil {
		return nil, err
	}
	return jsonExtraData, nil
}

func move(src *os.File, dst string) error {
	if err := src.Sync(); err != nil { // fsync on file
		return err
	}

	if err := os.Rename(src.Name(), dst); err != nil { // move file into storage pool
		return err
	}

	dirPath := path.Dir(dst)
	dir, err := os.Open(dirPath)
	if err != nil {
		return err
	}
	defer dir.Close()
	if err = dir.Sync(); err != nil { // fsync on destination directory
		return err
	}

	return nil
}

func allowed(remoteAddr string, allowedIPs []string) (bool, error) {
	ip, _, err := net.SplitHostPort(remoteAddr)
	if err != nil {
		return false, err
	}
	for _, cand := range allowedIPs {
		if ip == cand {
			return true, nil
		}
	}
	return false, nil
}

// store file under: basedir / pool / digest
func storeAndHash(rc io.ReadCloser, basedir string) ([]byte, error) {
	pool := path.Join(basedir, "pool")

	tmp, err := ioutil.TempFile(basedir, ".upload")
	if err != nil {
		return nil, err
	}
	defer os.Remove(tmp.Name())

	h := sha256.New()
	pr, pw := io.Pipe()
	tr := io.TeeReader(rc, pw)

	done := make(chan error)
	defer close(done)

	go func() { // write on disk
		defer pw.Close()
		written, err := io.Copy(tmp, tr)
		if err == nil && written == 0 {
			done <- errors.New("Empty file")
		} else {
			done <- err
		}
	}()

	go func() { // simultaneously compute hash
		_, err := io.Copy(h, pr)
		done <- err
	}()

	var failed error
	for c := 0; c < 2; c++ {
		err := <-done
		if err != nil {
			failed = err
		}
	}
	if failed != nil {
		return nil, failed
	}

	digest := h.Sum(nil)
	dest := path.Join(pool, hex.EncodeToString(digest))
	if err = move(tmp, dest); err != nil {
		return nil, err
	}

	return digest, nil
}

// create a trillian.LogLeaf ready for inclusion
func createLeaf(fileType api.FileType, fileHash []byte) (*trillian.LogLeaf, error) {
	v1 := api.LeafV1{
		FileType: fileType,
		FileHash: fileHash,
	}
	leaf := api.Leaf{
		Version: 1,
		V1:      &v1,
	}

	tlsLeafValue, err := tls.Marshal(leaf)
	if err != nil {
		return nil, err
	}

	jsonExtraData, err := now()
	if err != nil {
		return nil, err
	}

	logLeaf := trillian.LogLeaf{
		LeafValue:        tlsLeafValue,
		ExtraData:        jsonExtraData,
		LeafIdentityHash: fileHash, // each file can only be submitted under one file type and under one leaf version
	}
	return &logLeaf, nil
}

// issue a signed receipt based on a queued log leaf response
func createPromise(signer tcrypto.Signer, leaf trillian.LogLeaf) (*api.SubmitEntryResponse, error) {
	var returnedLeaf api.Leaf
	rest, err := tls.Unmarshal(leaf.LeafValue, &returnedLeaf)
	if err != nil {
		return nil, err
	} else if len(rest) > 0 {
		return nil, errors.New("Incomplete unmarshalling of leaf with index " + strconv.FormatInt(leaf.LeafIndex, 10))
	}

	var extraData extraData
	err = json.Unmarshal(leaf.ExtraData, &extraData)
	if err != nil {
		return nil, err
	}

	v1 := api.ReceiptV1{
		FileType:       tls.Enum(returnedLeaf.V1.FileType),
		FileHash:       returnedLeaf.V1.FileHash,
		TimestampNanos: extraData.TimestampNanos,
	}
	receipt := api.Receipt{
		Version: 1,
		V1:      &v1,
	}

	tlsReceipt, err := tls.Marshal(receipt)
	if err != nil {
		return nil, err
	}

	signature, err := signer.Sign(tlsReceipt)
	if err != nil {
		return nil, err
	}

	jsonRsp := api.SubmitEntryResponse{
		Receipt:          tlsReceipt,
		ReceiptSignature: signature,
	}
	return &jsonRsp, nil
}
