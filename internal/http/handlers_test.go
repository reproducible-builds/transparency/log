package http

import (
	"../../api"
	"bytes"
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/google/certificate-transparency-go/trillian/mockclient"
	"github.com/google/trillian"
	tcrypto "github.com/google/trillian/crypto"
	"github.com/google/trillian/types"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"strconv"
	"testing"
	"time"
)

func errPanic(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func newSigner() tcrypto.Signer {
	signer, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	errPanic(err)
	tsigner := tcrypto.NewSigner(0, signer, crypto.SHA256)
	return *tsigner
}

// callee must call teardown() afterwards
func setup(t *testing.T, signer tcrypto.Signer) (*gomock.Controller, *mockclient.MockTrillianLogClient, logInfo, context.Context) {
	ctrl := gomock.NewController(t)
	mc := mockclient.NewMockTrillianLogClient(ctrl)

	dir, err := ioutil.TempDir("", "")
	errPanic(err)
	li := logInfo{
		trillianClient: mc,
		logID:          666,
		timeout:        time.Second * 1,
		signer:         signer,
		storage:        dir,
		allowedIPs:     []string{"1.2.3.4"},
	}

	err = os.Mkdir(li.storage+"/pool", 0700)
	errPanic(err)

	ctx := context.Background()
	return ctrl, mc, li, ctx
}

func teardown(ctrl *gomock.Controller, li logInfo) {
	ctrl.Finish()
	os.RemoveAll(li.storage)
}

func errorCheck(t *testing.T, tcErr, gotErr error, tcRE, gotRE requestError) {
	if tcErr != nil && gotErr == nil {
		t.Errorf("want error: %v", tcErr)
	}
	if tcErr == nil && gotErr != nil {
		t.Errorf("unwanted error: %v", gotErr)
	}
	if tcErr != nil && gotRE != tcRE {
		t.Errorf("userFault got %v, but want %v", gotRE, tcRE)
	}
}

/* generic helpers above */

func createRoot(signer tcrypto.Signer) *trillian.SignedLogRoot {
	root := types.LogRootV1{
		TreeSize:       11,
		RootHash:       []byte{0x11},
		TimestampNanos: 11,
		Revision:       1,
	}

	bin, err := root.MarshalBinary()
	errPanic(err)
	signature, err := signer.Sign(bin)
	errPanic(err)

	signedRoot := trillian.SignedLogRoot{
		LogRoot:          bin,
		LogRootSignature: signature,
	}
	return &signedRoot
}

func createBadRoot(signer tcrypto.Signer) *trillian.SignedLogRoot {
	signedRoot := createRoot(signer)
	if signedRoot.LogRoot[0] == byte(0x00) {
		signedRoot.LogRoot[0] = byte(0x01)
	} else {
		signedRoot.LogRoot[0] = byte(0x00)
	}
	return signedRoot
}

func TestGetRoot(t *testing.T) {
	signer := newSigner()

	testCases := []struct {
		desc   string
		err    error
		re     requestError
		rpcRsp *trillian.GetLatestSignedLogRootResponse
		rpcErr error
	}{
		{
			desc:   "ok",
			err:    nil,
			re:     success,
			rpcRsp: &trillian.GetLatestSignedLogRootResponse{SignedLogRoot: createRoot(signer)},
			rpcErr: nil,
		},
		{
			desc:   "rpc error",
			err:    errors.New("some error"),
			re:     ourfault,
			rpcRsp: &trillian.GetLatestSignedLogRootResponse{},
			rpcErr: errors.New("some error"),
		},
		{
			desc:   "signature bad",
			err:    errors.New("some error"),
			re:     ourfault,
			rpcRsp: &trillian.GetLatestSignedLogRootResponse{SignedLogRoot: createBadRoot(signer)},
			rpcErr: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			ctrl, mc, li, ctx := setup(t, signer)
			defer teardown(ctrl, li)

			httpReq, err := http.NewRequest("GET", "", nil)
			errPanic(err)
			httpRec := httptest.NewRecorder()

			rpcReq := trillian.GetLatestSignedLogRootRequest{LogId: 666}
			mc.EXPECT().GetLatestSignedLogRoot(ctx, &rpcReq).Return(tc.rpcRsp, tc.rpcErr)

			gotRE, gotErr := getRoot(ctx, li, httpRec, httpReq)
			errorCheck(t, tc.err, gotErr, tc.re, gotRE)

			httpRsp := httpRec.Result()
			if tc.err == nil {
				exp := api.GetRootResponse{}
				err := json.NewDecoder(httpRsp.Body).Decode(&exp)
				if err != nil {
					t.Errorf("invalid response: %v %v", err, exp)
				}
			}
		})
	}
}

func TestGetConsProof(t *testing.T) {
	signer := newSigner()

	testCases := []struct {
		desc          string
		first, second int64
		err           error
		re            requestError
		rpcRsp        *trillian.GetConsistencyProofResponse
		rpcErr        error
	}{
		{
			desc:   "ok",
			first:  1,
			second: 66,
			err:    nil,
			re:     success,
			rpcRsp: &trillian.GetConsistencyProofResponse{
				Proof:         &trillian.Proof{Hashes: [][]byte{{0x11}}},
				SignedLogRoot: createRoot(signer),
			},
			rpcErr: nil,
		},
		{
			desc:   "rpc error",
			first:  2,
			second: 1,
			err:    errors.New("some error"),
			re:     ourfault,
			rpcRsp: &trillian.GetConsistencyProofResponse{},
			rpcErr: errors.New("some error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			ctrl, mc, li, ctx := setup(t, signer)
			defer teardown(ctrl, li)

			httpReq, err := http.NewRequest("GET", "", nil)
			errPanic(err)
			httpRec := httptest.NewRecorder()

			params := map[string][]string{
				"first":  {strconv.FormatInt(tc.first, 10)},
				"second": {strconv.FormatInt(tc.second, 10)},
			}
			httpReq.Form = params

			rpcReq := trillian.GetConsistencyProofRequest{
				LogId:          666,
				FirstTreeSize:  tc.first,
				SecondTreeSize: tc.second,
			}
			mc.EXPECT().GetConsistencyProof(ctx, &rpcReq).Return(tc.rpcRsp, tc.rpcErr)

			gotRE, gotErr := getConsProof(ctx, li, httpRec, httpReq)
			errorCheck(t, tc.err, gotErr, tc.re, gotRE)

			httpRsp := httpRec.Result()
			if tc.err == nil {
				exp := api.GetConsProofResponse{}
				err := json.NewDecoder(httpRsp.Body).Decode(&exp)
				if err != nil {
					t.Errorf("invalid response: %v %v", err, exp)
				}
			}
		})
	}
}

func TestGetInclProof(t *testing.T) {
	signer := newSigner()

	testCases := []struct {
		desc   string
		size   int64
		hash   []byte
		err    error
		re     requestError
		rpcRsp *trillian.GetInclusionProofByHashResponse
		rpcErr error
	}{
		{
			desc: "ok",
			size: 10,
			hash: []byte{0x05},
			err:  nil,
			re:   success,
			rpcRsp: &trillian.GetInclusionProofByHashResponse{
				Proof:         []*trillian.Proof{{Hashes: [][]byte{{0x11}}}},
				SignedLogRoot: createRoot(signer),
			},
			rpcErr: nil,
		},
		{
			desc:   "rpc error",
			size:   10,
			hash:   []byte{0x05},
			err:    errors.New("some error"),
			re:     ourfault,
			rpcRsp: &trillian.GetInclusionProofByHashResponse{},
			rpcErr: errors.New("some error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			ctrl, mc, li, ctx := setup(t, signer)
			defer teardown(ctrl, li)

			httpReq, err := http.NewRequest("GET", "", nil)
			errPanic(err)
			httpRec := httptest.NewRecorder()

			params := map[string][]string{
				"tree-size": {strconv.FormatInt(tc.size, 10)},
				"leaf-hash": {base64.StdEncoding.EncodeToString(tc.hash)},
			}
			httpReq.Form = params

			rpcReq := trillian.GetInclusionProofByHashRequest{
				LogId:    666,
				LeafHash: tc.hash,
				TreeSize: tc.size,
			}
			mc.EXPECT().GetInclusionProofByHash(ctx, &rpcReq).Return(tc.rpcRsp, tc.rpcErr)

			gotRE, gotErr := getInclProof(ctx, li, httpRec, httpReq)
			errorCheck(t, tc.err, gotErr, tc.re, gotRE)

			httpRsp := httpRec.Result()
			if tc.err == nil {
				exp := api.GetInclProofResponse{}
				err := json.NewDecoder(httpRsp.Body).Decode(&exp)
				if err != nil {
					t.Errorf("invalid response: %v %v", err, exp)
				}
			}
		})
	}
}

func TestGetEntries(t *testing.T) {
	signer := newSigner()

	testCases := []struct {
		desc       string
		start, end int64
		err        error
		re         requestError
		rpcRsp     *trillian.GetLeavesByRangeResponse
		rpcErr     error
	}{
		{
			desc:  "ok",
			start: 10,
			end:   12,
			err:   nil,
			re:    success,
			rpcRsp: &trillian.GetLeavesByRangeResponse{
				Leaves:        []*trillian.LogLeaf{{MerkleLeafHash: []byte{0x11}}},
				SignedLogRoot: createRoot(signer),
			},
			rpcErr: nil,
		},
		{
			desc:   "rpc error",
			start:  12,
			end:    10,
			err:    errors.New("some error"),
			re:     ourfault,
			rpcRsp: &trillian.GetLeavesByRangeResponse{},
			rpcErr: errors.New("some error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			ctrl, mc, li, ctx := setup(t, signer)
			defer teardown(ctrl, li)

			httpReq, err := http.NewRequest("GET", "", nil)
			errPanic(err)
			httpRec := httptest.NewRecorder()

			params := map[string][]string{
				"start": {strconv.FormatInt(tc.start, 10)},
				"end":   {strconv.FormatInt(tc.end, 10)},
			}
			httpReq.Form = params

			rpcReq := trillian.GetLeavesByRangeRequest{
				LogId:      666,
				StartIndex: tc.start,
				Count:      tc.end - tc.start + 1,
			}
			mc.EXPECT().GetLeavesByRange(ctx, &rpcReq).Return(tc.rpcRsp, tc.rpcErr)

			gotRE, gotErr := getEntries(ctx, li, httpRec, httpReq)
			errorCheck(t, tc.err, gotErr, tc.re, gotRE)

			httpRsp := httpRec.Result()
			if tc.err == nil {
				exp := api.GetEntriesResponse{}
				err := json.NewDecoder(httpRsp.Body).Decode(&exp)
				if err != nil {
					t.Errorf("invalid response: %v %v", err, exp)
				}
			}
		})
	}
}

func TestGetFile(t *testing.T) {
	signer := newSigner()

	testCases := []struct {
		desc string
		hash []byte
		err  error
		re   requestError
	}{
		{
			desc: "ok",
			hash: []byte{0x01, 0x02, 0x03},
			err:  nil,
			re:   success,
		},
		{
			desc: "not-found",
			hash: []byte{0x01, 0x02},
			err:  errors.New("some error"),
			re:   notfound,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			ctrl, _, li, ctx := setup(t, signer)
			defer teardown(ctrl, li)

			ioutil.WriteFile(li.storage+"/pool/010203", []byte("asdf"), 0644)

			httpReq, err := http.NewRequest("GET", "", nil)
			errPanic(err)
			httpRec := httptest.NewRecorder()

			params := map[string][]string{
				"file-hash": {base64.StdEncoding.EncodeToString(tc.hash)},
			}
			httpReq.Form = params

			gotRE, gotErr := getFile(ctx, li, httpRec, httpReq)
			errorCheck(t, tc.err, gotErr, tc.re, gotRE)

			httpRsp := httpRec.Result()
			if tc.err == nil {
				read, err := ioutil.ReadAll(httpRsp.Body)
				if err != nil || !bytes.Equal(read, []byte("asdf")) {
					t.Errorf("invalid response: %v %v", err, read)
				}
			}
		})
	}
}

func TestSubmitEntry(t *testing.T) {
	signer := newSigner()

	testCases := []struct {
		desc    string
		content []byte
		remote  string
		err     error
		re      requestError
		rpcRsp  *trillian.QueueLeavesResponse
		rpcErr  error
	}{
		{
			desc:    "ok",
			content: []byte{0x11, 0x00},
			remote:  "1.2.3.4:12345",
			err:     nil,
			re:      success,
			rpcRsp: &trillian.QueueLeavesResponse{
				QueuedLeaves: []*trillian.QueuedLogLeaf{
					{
						Status: status.New(codes.OK, "OK").Proto(),
					},
				},
			},
			rpcErr: nil,
		},
		{
			desc:    "empty",
			content: []byte{},
			remote:  "1.2.3.4:12345",
			err:     errors.New("some error"),
			re:      clientfault,
			rpcRsp:  &trillian.QueueLeavesResponse{},
			rpcErr:  nil,
		},
		{
			desc:    "rpc error",
			content: []byte{0x11, 0x00},
			remote:  "1.2.3.4:12345",
			err:     errors.New("some error"),
			re:      ourfault,
			rpcRsp:  &trillian.QueueLeavesResponse{},
			rpcErr:  errors.New("some error"),
		},
		{
			desc:    "denied",
			content: []byte{0x11, 0x00},
			remote:  "8.8.8.8:80",
			err:     errors.New("some error"),
			re:      denied,
			rpcRsp:  &trillian.QueueLeavesResponse{},
			rpcErr:  nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			ctrl, mc, li, ctx := setup(t, signer)
			defer teardown(ctrl, li)

			fileHash := sha256.Sum256(tc.content)

			leaf, _ := createLeaf(api.NotRelease, fileHash[:])
			leaf.LeafIndex = 1

			if tc.rpcRsp.QueuedLeaves != nil {
				tc.rpcRsp.QueuedLeaves[0].Leaf = leaf
			}
			if tc.desc != "empty" && tc.desc != "denied" {
				mc.EXPECT().QueueLeaves(ctx, gomock.Any()).Return(tc.rpcRsp, tc.rpcErr)
			}

			httpReq, err := http.NewRequest("POST", "", nil)
			errPanic(err)
			httpReq.RemoteAddr = tc.remote

			httpReq.Body = (ioutil.NopCloser(bytes.NewReader(tc.content)))

			httpRec := httptest.NewRecorder()

			gotRE, gotErr := submitEntry(ctx, li, httpRec, httpReq)
			errorCheck(t, tc.err, gotErr, tc.re, gotRE)

			httpRsp := httpRec.Result()
			if tc.err == nil {
				exp := api.GetEntriesResponse{}
				err := json.NewDecoder(httpRsp.Body).Decode(&exp)
				if err != nil {
					t.Errorf("invalid response: %v %v", err, exp)
				}

				fileName := hex.EncodeToString(fileHash[:])
				content, err := ioutil.ReadFile(li.storage + "/pool/" + fileName)
				if err != nil || !reflect.DeepEqual(tc.content, content) {
					t.Errorf("bad storage: %v", err)
				}
			}
		})
	}
}
