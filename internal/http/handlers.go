package http

import (
	"../../api"
	"context"
	"crypto"
	"encoding/hex"
	"encoding/json"
	"errors"
	"github.com/google/trillian"
	tcrypto "github.com/google/trillian/crypto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"strings"
	"time"
)

type requestError int

const (
	success requestError = iota
	ourfault
	clientfault
	notfound
	denied
)

// return an indication if we believe the client to be at fault
type handler func(context.Context, logInfo, http.ResponseWriter, *http.Request) (requestError, error)

type logInfo struct {
	trillianClient trillian.TrillianLogClient
	logID          int64
	timeout        time.Duration
	signer         tcrypto.Signer
	storage        string
	allowedIPs     []string
}

type appHandler struct {
	logInfo logInfo
	handler handler
	path    string
	method  string
}

func getRoot(ctx context.Context, li logInfo, w http.ResponseWriter, r *http.Request) (requestError, error) {
	req := trillian.GetLatestSignedLogRootRequest{
		LogId: li.logID,
	}
	rsp, err := li.trillianClient.GetLatestSignedLogRoot(ctx, &req)
	if err != nil {
		return ourfault, err
	}

	pubkey := li.signer.Public() // Trillian demands signature verification
	_, err = tcrypto.VerifySignedLogRoot(pubkey, crypto.SHA256, rsp.SignedLogRoot)
	if err != nil {
		return ourfault, err
	}

	jsonRsp := api.GetRootResponse{
		LogRoot:          rsp.SignedLogRoot.LogRoot,
		LogRootSignature: rsp.SignedLogRoot.LogRootSignature,
	}

	jsonData, err := json.MarshalIndent(&jsonRsp, "", "    ")
	if err != nil {
		return ourfault, err
	}
	w.Write(jsonData)
	return success, nil
}

func getConsProof(ctx context.Context, li logInfo, w http.ResponseWriter, r *http.Request) (requestError, error) {
	var urlReq api.GetConsProofRequest
	err := parseForm(r, &urlReq)
	if err != nil {
		return clientfault, err
	}

	req := trillian.GetConsistencyProofRequest{
		LogId:          li.logID,
		FirstTreeSize:  urlReq.First,
		SecondTreeSize: urlReq.Second,
	}
	rsp, err := li.trillianClient.GetConsistencyProof(ctx, &req)
	if err != nil {
		s := status.Convert(err)
		if s.Code() == codes.InvalidArgument {
			return clientfault, errors.New(s.Message())
		}
		return ourfault, err
	}

	if rsp.Proof == nil { // Second > current TreeSize
		return clientfault, errors.New("Parameter error")
	}

	pubkey := li.signer.Public() // Trillian demands signature verification
	_, err = tcrypto.VerifySignedLogRoot(pubkey, crypto.SHA256, rsp.SignedLogRoot)
	if err != nil {
		return ourfault, err
	}

	jsonRsp := api.GetConsProofResponse{
		AuditPath: rsp.Proof.Hashes,
	}

	jsonData, err := json.MarshalIndent(&jsonRsp, "", "    ")
	if err != nil {
		return ourfault, err
	}
	w.Write(jsonData)
	return success, nil
}

func getInclProof(ctx context.Context, li logInfo, w http.ResponseWriter, r *http.Request) (requestError, error) {
	var urlReq api.GetInclProofRequest
	err := parseForm(r, &urlReq)
	if err != nil {
		return clientfault, err
	}

	req := trillian.GetInclusionProofByHashRequest{
		LogId:    li.logID,
		LeafHash: urlReq.LeafHash,
		TreeSize: urlReq.TreeSize,
	}
	rsp, err := li.trillianClient.GetInclusionProofByHash(ctx, &req)
	if err != nil {
		s := status.Convert(err)
		if s.Code() == codes.NotFound {
			return clientfault, errors.New(s.Message())
		}
		return ourfault, err
	}

	jsonRsp := api.GetInclProofResponse{
		LeafIndex: rsp.Proof[0].LeafIndex,
		AuditPath: rsp.Proof[0].Hashes, // if the log permitted duplicates there could be multiple proofs
	}

	jsonData, err := json.MarshalIndent(&jsonRsp, "", "    ")
	if err != nil {
		return ourfault, err
	}
	w.Write(jsonData)
	return success, nil
}

func getEntries(ctx context.Context, li logInfo, w http.ResponseWriter, r *http.Request) (requestError, error) {
	var urlReq api.GetEntriesRequest
	err := parseForm(r, &urlReq)
	if err != nil {
		return clientfault, err
	}

	req := trillian.GetLeavesByRangeRequest{
		LogId:      li.logID,
		StartIndex: urlReq.Start,
		Count:      urlReq.End - urlReq.Start + 1,
	}
	rsp, err := li.trillianClient.GetLeavesByRange(ctx, &req)
	if err != nil {
		s := status.Convert(err)
		if s.Code() == codes.InvalidArgument {
			return clientfault, errors.New(s.Message())
		}
		return ourfault, err
	}

	entries := make([][]byte, len(rsp.Leaves))
	for i, l := range rsp.Leaves {
		entries[i] = l.LeafValue
	}
	jsonRsp := api.GetEntriesResponse{
		Entries: entries,
	}

	jsonData, err := json.MarshalIndent(&jsonRsp, "", "    ")
	if err != nil {
		return ourfault, err
	}
	w.Write(jsonData)
	return success, nil
}

func getFile(ctx context.Context, li logInfo, w http.ResponseWriter, r *http.Request) (requestError, error) {
	var urlReq api.GetFileRequest
	err := parseForm(r, &urlReq)
	if err != nil {
		return clientfault, err
	}

	hash := hex.EncodeToString(urlReq.FileHash)
	path := path.Join(li.storage, "pool", hash)
	f, err := os.Open(path)
	if err != nil {
		return notfound, err
	}

	_, err = io.Copy(w, f)
	if err != nil {
		return ourfault, err
	}
	return success, nil
}

// on resubmission we accept the complete upload again and sign the
// inclusion promise with the previous timestamp (like ctfe and gossip)
func submitEntry(ctx context.Context, li logInfo, w http.ResponseWriter, r *http.Request) (requestError, error) {
	perm, err := allowed(r.RemoteAddr, li.allowedIPs)
	if err != nil {
		return ourfault, err
	}
	if !perm {
		return denied, errors.New("permission denied: " + r.RemoteAddr)
	}

	fileHash, err := storeAndHash(r.Body, li.storage)
	if err != nil {
		if err.Error() == "Empty file" {
			return clientfault, err
		}
		return ourfault, err
	}
	log.Println("new submission: " + hex.EncodeToString(fileHash))

	fileType := api.NotRelease // default file type
	if strings.HasSuffix(r.URL.Path, "/submit-release") {
		fileType = api.Release
	}

	logLeaf, err := createLeaf(fileType, fileHash)
	if err != nil {
		return ourfault, err
	}

	req := trillian.QueueLeavesRequest{
		LogId:  li.logID,
		Leaves: []*trillian.LogLeaf{logLeaf},
	}
	rsp, err := li.trillianClient.QueueLeaves(ctx, &req)
	if err != nil {
		return ourfault, err
	}
	if rsp == nil || len(rsp.QueuedLeaves) != 1 {
		return ourfault, errors.New("strange reply on queuing leaf: " + hex.EncodeToString(fileHash))
	}

	// issue promise based on returned leaf, then we don't have to validate QueueLeaves response and can't make erroneous promises
	jsonRsp, err := createPromise(li.signer, *rsp.QueuedLeaves[0].Leaf)
	if err != nil {
		return ourfault, err
	}

	jsonData, err := json.MarshalIndent(jsonRsp, "", "    ")
	if err != nil {
		return ourfault, err
	}
	w.Write(jsonData)
	return success, nil
}

func (a *appHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println("request", r.URL)
	if r.Method != a.method {
		http.Error(w, "Method not allowed", 405)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	ctx, cancel := context.WithTimeout(r.Context(), a.logInfo.timeout*10) // XXX more sensible timeout control
	defer cancel()

	errType, err := a.handler(ctx, a.logInfo, w, r)
	if err != nil {
		if errType == clientfault {
			http.Error(w, err.Error(), 400)
		} else if errType == denied {
			log.Println(err)
			http.Error(w, err.Error(), 403)
		} else if errType == notfound {
			log.Println(err)
			http.Error(w, "Not found", 404)
		} else {
			log.Println(err)
			http.Error(w, "Internal server error", 500)
		}
	}
}

// SetupHandlers hooks in HTTP handlers for a given Trillian log
func SetupHandlers(trillianClient trillian.TrillianLogClient, logID int64, timeout time.Duration, signer tcrypto.Signer, storage string, allowedIPs []string) {
	logInfo := logInfo{
		trillianClient: trillianClient,
		logID:          logID,
		timeout:        timeout,
		signer:         signer,
		storage:        storage,
		allowedIPs:     allowedIPs,
	}

	mapped := []struct {
		path    string
		method  string
		handler handler
	}{
		{"get-root", http.MethodGet, getRoot},
		{"get-cons-proof", http.MethodGet, getConsProof},
		{"get-incl-proof", http.MethodGet, getInclProof},
		{"get-entries", http.MethodGet, getEntries},
		{"get-file", http.MethodGet, getFile},
		{"submit", http.MethodPost, submitEntry},
		// separate endpoint, otherwise we would need to have multipart encoding and store the file in temp files
		{"submit-release", http.MethodPost, submitEntry},
	}

	for _, mapped := range mapped {
		a := appHandler{
			handler: mapped.handler,
			method:  mapped.method,
			logInfo: logInfo,
		}
		http.Handle("/strans/v0/"+mapped.path, &a)
	}
}
