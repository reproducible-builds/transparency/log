package http

import (
	"encoding/base64"
	"errors"
	"net/http"
	"net/url"
	"reflect"
	"strconv"
)

func formParam(u url.Values, name string) (string, error) {
	values, ok := u[name]
	if !ok {
		return "", errors.New("Parameter not found: " + name)
	}
	if len(values) != 1 {
		return "", errors.New("Parameter missing or duplicate: " + name)
	}
	return values[0], nil
}

// cast value according to struct member type and assign it to the member
func setMember(typeField reflect.StructField, fieldValue reflect.Value, paramValue string) error {
	switch fieldValue.Kind() {
	case reflect.Int64:
		converted, err := strconv.ParseInt(paramValue, 10, 64)
		if err != nil {
			return err
		}
		fieldValue.SetInt(converted)
	case reflect.Uint64:
		converted, err := strconv.ParseUint(paramValue, 10, 64)
		if err != nil {
			return err
		}
		fieldValue.SetUint(converted)
	case reflect.Slice:
		if typeField.Type.Elem().Kind() != reflect.Uint8 {
			panic("Type not implemented")
		}
		converted, err := base64.StdEncoding.DecodeString(paramValue)
		if err != nil {
			return err
		}
		fieldValue.SetBytes(converted)
	default:
		panic("Type not implemented")
	}
	return nil
}

// fill pointed to struct with data parsed from form
func parseForm(r *http.Request, ptr interface{}) error {
	r.ParseForm()
	v := reflect.ValueOf(ptr).Elem()

	for i := 0; i < v.NumField(); i++ { // iterate through all members
		typeField := v.Type().Field(i)
		tag := typeField.Tag.Get("form")

		paramValue, err := formParam(r.Form, tag)
		if err != nil {
			return err
		}

		if err = setMember(typeField, v.Field(i), paramValue); err != nil {
			return err
		}

	}
	return nil
}
