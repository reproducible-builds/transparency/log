package api

import "github.com/google/certificate-transparency-go/tls"

// FileType distinguishes different types of leaves
type FileType tls.Enum

// We care to distinguish release files from the rest. Any subordinate files can be discovered by parsing the release file.
const (
	Release    FileType = 0
	NotRelease FileType = 65535
)

// LeafV1 contians the file type in order for monitors to detect releases
type LeafV1 struct {
	FileType FileType `tls:"size:2"`
	FileHash []byte   `tls:"minlen:0,maxlen:65535"`
}

// Leaf will be the Trillian LeafValue, i.e. the Merkle tree leaf
type Leaf struct {
	Version tls.Enum `tls:"size:2"`
	V1      *LeafV1  `tls:"selector:Version,val:1"`
}

// ReceiptV1 contains the file type in order to make it binding for the log
type ReceiptV1 struct {
	FileType       tls.Enum `tls:"size:2"`
	FileHash       []byte   `tls:"minlen:0,maxlen:65535"`
	TimestampNanos uint64
}

// Receipt contains a TLS serialised leaf hash and timestamped to be signed as inclusion promise
type Receipt struct {
	Version tls.Enum   `tls:"size:2"`
	V1      *ReceiptV1 `tls:"selector:Version,val:1"`
}

// GetRootResponse holds a signed log root
// LogRoot is a TLS serialised Trillian v1 root:
// struct {
//   uint64 tree_size;
//   opaque root_hash<0..128>;
//   uint64 timestamp_nanos;
//   uint64 revision;
//   opaque metadata<0..65535>;
// } LogRootV1;
//
// enum { v1(1), (65535)} Version;
// struct {
//   Version version;
//   select(version) {
//     case v1: LogRootV1;
//   }
// } LogRoot;
type GetRootResponse struct {
	LogRoot          []byte `json:"log_root"`
	LogRootSignature []byte `json:"log_root_signature"`
}

// GetConsProofRequest specifies tree sizes for a Merkle audit path request
type GetConsProofRequest struct {
	First  int64 `form:"first"`
	Second int64 `form:"second"`
}

// GetConsProofResponse holds a Merkle audit path and larger signed log root
type GetConsProofResponse struct {
	AuditPath [][]byte `json:"audit_path"`
}

// GetInclProofRequest specifies an audit path request by leaf hash and tree size
// LeafHash := sha256(0x00 || sha256(file))
type GetInclProofRequest struct {
	TreeSize int64  `form:"tree-size"`
	LeafHash []byte `form:"leaf-hash"`
}

// GetInclProofResponse holds a Merkle audit path
type GetInclProofResponse struct {
	LeafIndex int64    `json:"leaf_index"`
	AuditPath [][]byte `json:"audit_path"`
}

// GetEntriesRequest specifies a leaf range (inclusive) to retrieve
type GetEntriesRequest struct {
	Start int64 `form:"start"`
	End   int64 `form:"end"`
}

// GetEntriesResponse hold leaves
type GetEntriesResponse struct {
	Entries [][]byte `json:"entries"`
}

// GetFileRequest specifies a leaf number
type GetFileRequest struct {
	FileHash []byte `form:"file-hash"`
}

// SubmitEntryResponse holds an inclusion promise: time stamp, leaf hash, signature
type SubmitEntryResponse struct {
	Receipt          []byte `json:"receipt"`
	ReceiptSignature []byte `json:"receipt_signature"`
}
