package main

import (
	ihttp "../../internal/http"
	"crypto"
	"flag"
	"github.com/google/trillian"
	tcrypto "github.com/google/trillian/crypto"
	"github.com/google/trillian/crypto/keys/pem"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"os"
	"path"
	"strings"
	"time"
)

var (
	listenFlag        = flag.String("listen", "[::]:443", "Port to serve apt log requests on")
	storageFlag       = flag.String("storage", "/srv/tapt/", "directory to store uploaded blobs")
	backendTreeIDFlag = flag.Int64("log_tree", 0, "TreeID of the log to use on the backend server")
	rpcBackendFlag    = flag.String("log_rpc_server", "localhost:8090", "Backend specification; comma-separated list")
	rpcTimeoutFlag    = flag.Duration("log_rpc_timeout", time.Second*10, "Timeout for backend RPC requests")
	logKeyFlag        = flag.String("log_key", "/etc/tapt/log.privkey.pem", "PEM Private key used for log signing")
	keyPassphraseFlag = flag.String("log_key_passphrase", "", "Passphrase to decrypt the log key")
	tlsKeyFlag        = flag.String("tls_key", "/etc/ssl/private/ssl-cert-snakeoil.key", "TLS private key")
	tlsCertFlag       = flag.String("tls_certificate", "/etc/ssl/certs/ssl-cert-snakeoil.pem", "TLS certificate")
	allowedIPsFlag    = flag.String("allowed_ips", "::1", "Submission IP addresses")
)

// clean the path and test if we can create files there
func setupStorage(storage string) (string, error) {
	dir := path.Clean(storage)

	if err := os.MkdirAll(dir+"/pool", 0755); err != nil {
		return "", err
	}

	filePath := path.Join(storage, ".test")
	file, err := os.Create(filePath)

	if err != nil {
		return "", err
	}
	if err = file.Close(); err != nil {
		return "", err
	}
	return dir, os.Remove(filePath)
}

func main() {
	flag.Parse()

	conn, err := grpc.Dial(*rpcBackendFlag, grpc.WithInsecure(), grpc.WithTimeout(*rpcTimeoutFlag))
	if err != nil {
		log.Fatalf("Failed to connect to log server: %v", err)
	}
	defer conn.Close()

	trillianClient := trillian.NewTrillianLogClient(conn)

	signer, err := pem.ReadPrivateKeyFile(*logKeyFlag, *keyPassphraseFlag)
	if err != nil {
		log.Fatalf("Failed to load key: %v", err)
	}
	tsigner := tcrypto.NewSigner(*backendTreeIDFlag, signer, crypto.SHA256)

	dir, err := setupStorage(*storageFlag)
	if err != nil {
		log.Fatalf("Storage not usable: %v", err)
	}

	ips := strings.Split(*allowedIPsFlag, ",")

	ihttp.SetupHandlers(trillianClient, *backendTreeIDFlag, *rpcTimeoutFlag, *tsigner, dir, ips)

	log.Printf("Server initialised")
	err = http.ListenAndServeTLS(*listenFlag, *tlsCertFlag, *tlsKeyFlag, nil)
	log.Printf("Server shutting down: %v", err)
}
